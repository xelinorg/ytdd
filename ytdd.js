const path = require('path')
const ytdl = require('ytdl-core')
const ffmpeg = require('fluent-ffmpeg')
const fs = require('fs')

// private properties
function logProcess (option) {
  return function () {
    process.stdout.write(option + arguments[1]/arguments[2]*100 + '\r')
  }
}

function chooseAudio (format, selectedContainer) {
  return format.container === selectedContainer && format.mimeType.indexOf('audio') === 0 && !format.qualityLabel
}

function chooseVideo (format, selectedContainer, selectedQualityLevel) {
  return format.container === selectedContainer && format.mimeType.indexOf('video') === 0 && selectedQualityLevel.indexOf(format.qualityLabel) >= 0
}

function filterFormatAudio (selectedContainer, url) {
  // audio formats have audioQuality and should not have qualityLabel, all have container and must be selected and match
  return function (format) {
    const choosenAudio = chooseAudio(format, selectedContainer)
    if (choosenAudio) {
      console.log('choosenAudio format is :', url, format)
    }
    return choosenAudio
  }
}

function filterFormatVideo (selectedContainer, selectedQualityLevel, url) {
  // video formats have qualityLabel and must be set, all have container and must be selected and match
  return function (format) {
    const choosenVideo = chooseVideo(format, selectedContainer, selectedQualityLevel)
    if (choosenVideo) {
      console.log('choosenVideo format is :', url, format)
    }
    return choosenVideo
  }
}

function getOutPaths (fileType, outname) {
  const datapath = './data/'
  if (fileType === 'main') {
    return path.resolve(datapath, outname + '.mp4')
  } else if ( fileType === 'video' ) {
    return path.resolve(datapath, outname + '.video.mp4')
  } else if (fileType === 'audio') {
    return path.resolve(datapath, outname + '.sound.mp4')
  }
}


// exposed properties
function getInfo (url, option, verbose, callback) {
  verbose && console.log('getInfo option is :\n', option, url)
  const cb = callback || (() => {})

  ytdl.getInfo(url, option, function (err, info) {
    if (err) return cb(err)

    verbose && console.log('info keys :\n', url, Object.keys(info))
    if (option && option.keys) {
      option.keys.forEach( function (key) {
        verbose && console.log(url, info[key])
      })
    }
    cb(null, info)
  })
}

function getSoundAndVideo (url, container, quality, audioOutput, videoOutput, foundFormats, cb) {
  if (foundFormats.audio){
    const audio = ytdl(url, { filter: filterFormatAudio(container, url)})
    audio.on('progress', logProcess(url + ' audio progress '))
      .pipe(fs.createWriteStream(audioOutput))
  } else {
    console.log('getSoundAndVideo no foundFormats.audio ', foundFormats)
  }

  if (foundFormats.video){
    const video = ytdl(url, { filter: filterFormatVideo(container, quality, url)})
    video.on('progress', logProcess(url + ' video progress '))
      .pipe(fs.createWriteStream(videoOutput))
  }  else {
    console.log('getSoundAndVideo no foundFormats.video ', foundFormats)
  }

}

function doNext (url, container, quality, audioOutput, mainOutput, foundFormats, cb) {
  ytdl(url, { filter: filterFormatAudio(container, url)})
    .on('error', console.error)
    .on('progress', logProcess('audio progress '))

    // Write audio to file since ffmpeg supports only one input stream.
    .pipe(fs.createWriteStream(audioOutput))
    .on('finish', () => {
      const video = ytdl(url, { filter: filterFormatVideo(container, quality, url)})
      video.on('progress', logProcess('video progress '))
      ffmpeg()
        .input(video)
        .videoCodec('copy')
        .input(audioOutput)
        .audioCodec('copy')
        .save(mainOutput)
        .on('error', console.error)
        .on('end', () => {
          fs.unlink(audioOutput, err => {
            if (err) console.error(err)
            else console.log(`\nfinished downloading, saved to ${mainOutput}`)
          })
        })
    })
}

module.exports = {
  getInfo: getInfo,
  getSoundAndVideo: function (url, outname, selectedContainer, selectedQualityLevel) {
    return getInfo(url, {}, false, function(err, info) {
      const videoOutput = getOutPaths('video', outname)
      const audioOutput = getOutPaths('audio', outname)
      const container = selectedContainer || 'mp4'
      const quality = selectedQualityLevel || '360p, 480p, 720p'

      const foundFormats = {
        audio: info.formats.find(function(format){ return format.container === selectedContainer && !format.qualityLabel}),
        video: info.formats.find(function(format){ return format.container === selectedContainer && quality.indexOf(format.qualityLabel) >= 0})
      }
      return getSoundAndVideo(url, container, quality, audioOutput, videoOutput, foundFormats)
    })

  },
  doNext: function (url, outname, selecteContainer, selectedQualityLevel) {
    return getInfo(url, {}, false, function(err, info) {
      const mainOutput = getOutPaths('main', outname)
      const audioOutput = getOutPaths('audio', outname)
      const container = selecteContainer || 'mp4'
      const quality = selectedQualityLevel || '360p, 480p, 720p'

      const foundFormats = {
        audio: info.formats.find(function(format){ return format.container === selecteContainer && !format.qualityLabel}),
        video: info.formats.find(function(format){ return format.container === selecteContainer && quality.indexOf(format.qualityLabel) >= 0})
      }
      if (foundFormats.audio && foundFormats.video) {
        return doNext(url, container, quality, audioOutput, mainOutput, foundFormats)
      } else {
        console.log('doNext needs audio and video, not found both NO doNext', foundFormats)
        return
      }

    })
  }
}
