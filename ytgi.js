const https = require('https')
const querystring = require('querystring');

const INFO_HOST = 'www.youtube.com';
const INFO_PATH = '/get_video_info';


function fixPath (basePath, vid) {
  return basePath + '?video_id=' + vid
}

function getOptions (vid) {
  return {
    hostname: INFO_HOST,
    port: 443,
    path: fixPath(INFO_PATH, vid),
    method: 'GET'
  }
}

function getInfo (vid) {
  const options = getOptions(vid)
  return new Promise((resolve, reject) => {
    const req = https.request(options, (res) => {

      let dataBox = ''
      res.setEncoding('utf8')
      res.on('data', (chunk) => {
        dataBox = dataBox + chunk
      })
      res.on('end', () => {
        const nice = {}
        const cleandata = querystring.parse(dataBox)

        Object.keys(cleandata).reduce((acc, curkey) => {
          if (curkey === 'player_response' ) {
            acc[curkey] = JSON.parse(cleandata[curkey])
          } else {
            acc[curkey] = cleandata[curkey]
          }
          return acc
        }, nice)
        return resolve(nice)
      })
    })

    req.on('error', (e) => {
      return reject(e)
    })
    
    req.end()
  })
}

module.exports.getInfo = getInfo
