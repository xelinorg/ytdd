const fs = require('fs')
const readline = require('readline')

const ytdd = require('./ytdd.js')
const ytGetinfo= require('./ytgi.js').getInfo


let justgetinfo = process.argv.find(arg => arg.indexOf('getinfo=') === 0)
if (justgetinfo ) {
  justgetinfo = justgetinfo.replace('getinfo=', '')
  const infokeys = justgetinfo.match(/([a-zA-Z09_]{10})[\s\S/]/)
  console.log('getinfo infokeys', infokeys)
  if (infokeys.length) {
    ytGetinfo(infokeys[0]).then(info=>{
      console.log(info)
    })  
  }
  return
}

const pattern = /^(\d*).\s*([\W\S]*)\s(https[\w\W]*)\s\d*\.\d*/

const settings = process.argv.reduce(function (bucket, arg) {
  if (arg.indexOf('=') > 0 && arg.length > 2) {
    const pair = arg.split('=')
    bucket[pair[0]] = pair[1]
  }
  return bucket
}, {})

if (settings.in) {
  console.log('command line settings :\n', settings)
  var reader = readline.createInterface({
    input: fs.createReadStream(settings.in),
    terminal: false
  })

  reader.on('line', function (line) {
    console.log('line is:\n', line)
    const splittedLine = line.split(pattern)
    if (settings.info && splittedLine.length > 3) {
      const infoopt = JSON.parse(settings.info)
      const verbose = settings.verbose
      ytdd.getInfo(splittedLine[3], infoopt, verbose)
    } else if (splittedLine.length > 3) {
      const quality = settings.quality || '360p'
      const container = settings.container || 'mp4'
      console.log('final settings:\n', settings)
      if (settings.nojoin) {
        return ytdd.getSoundAndVideo(splittedLine[3], splittedLine[2].split(' ').join('_'), container, quality)
      }
      return ytdd.doNext(splittedLine[3], splittedLine[2].split(' ').join('_'), container, quality)
    }
  })
}

// source list file line format
// 1. Motivational short video  How to succeed  cartoon https://www.youtube.com/watch?v=hS5CfP8n_js 0.55
// 2. Catolog Tik Tok Funny Videos https://www.youtube.com/watch?v=WP_E_Mhz_s8 0.10
// ^http:\/\/(?:www\.)?youtube.com\/watch\?v=\w+(&\S*)?$

// get information filtered with the info key
// node unlist.js in=./download_example.txt info={\"keys\":\ \[\"formats\"\]} verbose=1

// download both and save without passing to ffmpeg
// node unlist.js in=./download_example.txt nojoin=1 quality=480p,720p container=mp4

// download both and pass to ffmpeg and then delete the sound
// node unlist.js in=./download_example.txt quality=360p,480p,720p container=mp4
